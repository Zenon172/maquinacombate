# MaquinaDeCombate

![War_Machine](https://1.bp.blogspot.com/-JV7B-QK6k5U/WrVQ6VVW03I/AAAAAAABQhY/M9aY9P8VO6IMbGet4bFmCIZCJdcWDDUPgCKgBGAs/s1600/IMG_9066.JPG)

MaquinaDeCombate é um robocode feito em Java por Lincoln Silva de Oliveira

## Repositório:

Repositorio [gitlab](https://gitlab.com/Zenon172/maquinacombate.git) 


## Funcionalidade:
MaquinaDeCombate inicia fazendo um scan com Giro de 360° para encontrar um inimigo e fixar nele, quando encontra um inimigo ele direciona o radar e o canhão em direção no inimigo e avança, caso ele receba um tiro o mesmo muda o inimigo para quem atirou e muda a diração, a intensidade do tiro depende da distância do inimigo o que economiza energia pois a possibilidade de errar um inimigo longe é maior.
acredito que o ponto fraco seja a a movimentação pois por ele focar no inimigo ele não se movimenta defensivamente.
### Aprendizado:
Foi muito interessante ter revivido a experiência de utilizar o Robocode, tive um pouco de contato na faculdade porém agora estudei mais, li a documentação e intendi muita coisa que não conhecia.